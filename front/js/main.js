const ranking = new Ranking('#numbers-ranking');
const random = new Random('#random-numbers');
random.init();
ranking.init();
window.onload = function() {
    setCounters();
    searchForSameAndSort();
};

//setting fetching interval
setInterval( function() {
    fetchNumbers(random);
    setTimeout(function() {
        searchForSameAndSort();
    }, 100);
}, 10000);

//fetching random numbers function
function fetchNumbers(selector) {
    selector.getDOMElement().innerHTML = "";
    selector.init();
}

//seting counters to 0 on the beginning
function setCounters() {
    const counters = document.getElementsByClassName('counter');
    Array.prototype.forEach.call( counters, counter => {
        counter.innerHTML = 0;
    });
}

//comparing randoms with ranking and sorting
function searchForSameAndSort() {
    const rankNumbers = document.getElementById('numbers-ranking').children;
    const randomNumbers = document.getElementById('random-numbers').children;

    //search and count
    Array.prototype.forEach.call( rankNumbers, singleNumber => {
        //define number and counter inside li
        const num = singleNumber.children[0].innerHTML;
        let counter = parseInt(singleNumber.children[1].innerHTML);

        //count how many times number appears in Randoms card
        Array.prototype.forEach.call( randomNumbers, singleRandNumber => {
            randNum = singleRandNumber.innerHTML;
            if ( num === randNum ) {
                counter++;
            }
        });

        //increase counter on front
        singleNumber.children[1].innerHTML = counter;
    });

    //sort
    sortRanking(document.getElementById('numbers-ranking'));
    
}

//sorting helper function
function sortRanking(ul){
    var newUl = ul.cloneNode(false);

    //add numbers lis to an array
    var elements = [];
    for ( var i = ul.childNodes.length; i--; ) {
        if( ul.childNodes[i].nodeName === 'LI' )
            elements.push( ul.childNodes[i] );
    }

    //sort by counter value
    elements.sort( function(a, b) {
       return parseInt(b.children[1].innerHTML) - 
              parseInt(a.children[1].innerHTML);
    });

    
    for ( var i = 0; i < elements.length; i++ )
        newUl.appendChild(elements[i]);
    ul.parentNode.replaceChild(newUl, ul);
}